---
layout: handbook-page-toc
title: "Community Advocacy"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

> The Community Advocates Program has been dissolved. Thank you for your patience while we work to deprecate these handbook pages. Please reach out to @community-team on slack if you need the Community Team's assistance. Also check out the [Community Operations handbook page](https://about.gitlab.com/handbook/marketing/community-relations/community-operations/).

## Who we are

<%= direct_team(manager_role: 'Sr. Community Advocates Manager') %>

## Finding the Community Advocates

- [**Community Advocacy Issue Tracker**](https://gitlab.com/gitlab-com/marketing/community-relations/community-advocacy/general/issues); please use confidential issues for topics that should only be visible to team members at GitLab.
- [**Chat channel**](https://gitlab.slack.com/messages/community-relations); please use the `#community-advocates` chat channel for questions that don't seem appropriate to use the issue tracker for.
- [**Community Advocates on GitLab.com**](https://gitlab.com/community-advocates): Use the `@community-advocates` handle in GitLab.com to mention the team for comments in issues or MRs that require their attention.

### Emergency contact

- [**Advocates contact**](https://docs.google.com/document/d/1rjEKcIUC2H1HmPuQyijllVu044RxaEnRXH3GQKCsKyI/edit#heading=h.r01iolr373k3)
- [**Incident management roles**](/handbook/engineering/infrastructure/incident-management/#roles-and-responsibilities)
- **Relevant Slack channels for incident/infrastructure:** [`#incident-management`](https://gitlab.slack.com/messages/incident-management), [`#infrastructure-lounge`](https://gitlab.slack.com/messages/incident-management)

## <i class="fas fa-book fa-fw icon-color font-awesome" aria-hidden="true"></i> Community Advocate Resources

- Community Advocate Bootcamp
  - [Bootcamp](/handbook/marketing/community-relations/community-advocacy/onboarding/bootcamp/)

----

## Role of Community Advocacy

### Goal

The goal of community advocacy is to respond to all of the GitLab mentions and questions asked online in a timely manner.

### Plan

1. Have discount codes that are easily distributed by team members
1. Send every major contributor a personalized gift
1. Expand the coverage of all GitLab mentions.
1. Improve responsiveness on high priority channels.
1. Do the rest of the [contributor journey](/handbook/journeys/#contributor-journey)

### Vision

1. GitLab has 1000's of active content contributors (e.g. for blogs, meetups, presentations, etc.)
1. Being a core contributor is a very rewarding experience
1. There are 10's of active GitLab/[ConvDev](http://conversationaldevelopment.com/) meet-ups
1. 100's of talks per year given at conferences and meetups
1. Our most active content contributors come to our summits
1. 100's of people contribute content about GitLab every month
1. We use software that helps us to keep track of core contributors (can be forum, Highrise, software made for advocacy, or a custom Rails app)
1. There is a core contributors page organized per region with the same information as the [team page](/company/team/) and what they contributed, where they work (if they have a LinkedIn profile), and a button to sent them an email via a form.
1. We measure and optimize every step of the [contributor journey](/handbook/journeys/#contributor-journey)

### Respond to community questions about GitLab asked online

- This includes helping members of the community with _their_ questions, but also making sure that the community is heard and that the feedback from the community reaches the rest of the team at GitLab.
- Engage with the developer community in a way that is direct but friendly and authentic. Be able to carry the tone of the GitLab brand while also giving the proper answers or direction to members of the community.
- [Engage with experts in the GitLab team](/handbook/marketing/community-relations/community-advocacy/workflows/involving-experts/) to provide the best quality answers and to expose them to community feedback.
- Help update the [social media guidelines](/handbook/marketing/team-member-social-media-policy/) and GitLab voice as new situations arise.
- Explore different tools from Zendesk to Mentions to find a way to track all mentions of GitLab across the internet.
- Don’t be afraid of animated gifs and well-placed humor! We are not robots.
- Work within the GitLab process to help users report bugs, make feature requests, contact support, and provide feedback on the product.

### Respond to every question asked internally

- This includes helping GitLab team members with _their_ questions across the `#community-advocates`, `#community-relations`, `#community-programs`, `#swag` Slack channels and making sure that they are heard, and that we help them with our input and assistance.
- Make sure to enable the Slack notifications for all the new messages in the channels mentioned above.
- Help the team with tailoring and reviewing the drafts of their responses going online.

## Involve experts

As Community Advocates, we will often want to involve experts in a topic being discussed online. The [Involving experts workflow section](/handbook/marketing/community-relations/community-advocacy/workflows/involving-experts/) describes how we do it.

### Can you please respond to this?

You got a link to this because we'd like you to respond to the mentioned community comment. We want to make sure we give the best answer possible by connecting the wider community with our experts and expose you to more community feedback.

#### Expert Response Guidelines

* When responding to community mentions, you should check out the [social media guidelines](/handbook/marketing/team-member-social-media-policy/).
* Since the community members can't participate in the internal Slack discussions, please answer in the social channel that the comment was originally posted. This builds community directly between GitLab team members and the wider community.
* If you can't respond to the linked comment, that's OK, but please quickly let the person who pinged you know so they can ping someone else. Just sharing a project or an issue your team has on the horizon can also be an effective response.
* Sometimes, a simple 'thanks' or recognition is enough.
* We value efficient communication, but this could come across as short to the wider community. Think about tone and authenticity when responding.

#### Expert Response Steps

* Consider the best [response strategy](/handbook/marketing/community-relations/community-advocacy/#expert-response-strategies) and navigate to the comment provided by the Advocate.
* Respond on the related social channel using your individual account.
   - When responding on [news.ycombinator.com](https://news.ycombinator.com/), please make sure the `about` field on your account page is representing your current role at GitLab.
   - When responding on our website comments, please use your personal Disqus account.
   - We can upgrade your [GitLab forum](http://forum.gitlab.com/) account to Admin if needed.
* Let the Advocate know that you responded on Slack, so they can close the related ticket on Slack.
* No further action is required from your side - advocates will notify you again if needed.

#### Expert Response Strategies

If you or your team is unsure how to best collaborate on a community response, consider using one of these strategies:

* Discuss potential responses on the Slack thread where a Community Advocate has pinged you. Advocates can jump in and help with the response.
* Create a collaborative Google Doc to draft and plan response with team members asynchronously. See [this example](https://docs.google.com/document/d/1GGO2kef7uTHxrBYVi4lUo8fbiIThggv1OfLLAtqPzzU/edit) from experts on the Manage team whose collaboration resulted in a [personal and detailed Twitter response](https://twitter.com/iamyoginth/status/1204293292893949952).
* Learn more about building community by attending the Community Relations monthly Book Club or reading about our discussion in [this issue](https://gitlab.com/gitlab-com/marketing/community-relations/community-advocacy/general/issues/63)
* If you're part of the customer success team, consider adding your social responses to this [shared snippet page for social responses](https://gitlab.com/guided-explorations/shared-snippets/snippets)

## Community response channels

The Community Advocates actively monitor and respond to the following set of channels.

In this overview:
- Those channels not marked as active need a response workflow to be put in place and are currently monitored on an occasional basis.
- Each channel has a link to the workflow in place to process responses.

| CHANNEL | SOURCE | AUTOMATION | DESTINATION | ACTIVE? |
| - | - | - | - | - |
| [`@gitlab`](/) | Twitter mentions | Zendesk | Zendesk | ✓ |
| [`@movingtogitlab`](/handbook/marketing/community-relations/community-advocacy/workflows/twitter/)  | Twitter mentions | Zendesk | Tweetdeck | ✓ |
| [`@gitlabstatus`](/handbook/marketing/community-relations/community-advocacy/workflows/twitter/)  | Twitter mentions | Zendesk | Zendesk | ✓ |
| [Facebook](/handbook/marketing/community-relations/community-advocacy/workflows/facebook/)  | Facebook page messages | Zapier | Zendesk | ✓ |
| [Hacker News](/handbook/marketing/community-relations/community-advocacy/workflows/hackernews/) | Hacker News mentions | Zapier | Zendesk and Slack: #hn-mentions | ✓ |
| [Hacker News front page stories](/handbook/marketing/community-relations/community-advocacy/workflows/hackernews/) | Hacker News front page mentions | Zapier | Slack: #community-advocates | ✓ |
| [Education Program](/handbook/marketing/community-relations/community-operations/community-program-applications/) | Education application form | Marketo | Salesforce and Zendesk | ✓ |
| [Open Source Program](/handbook/marketing/community-relations/community-operations/community-program-applications/) | Open Source application form | Marketo | Salesforce and Zendesk | ✓ |
| [Startups Program](/handbook/marketing/community-relations/community-operations/community-program-applications/) | Startup application form | Marketo | Salesforce and Zendesk | ✓ |
| [E-mail (merch@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail/) | Shop contact | E-mail alias | Zendesk | ✓ |
| [E-mail (community@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail/) | Handbook | E-mail alias | Zendesk | ✓ |
| [E-mail (movingtogitlab@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail/) | #movingtogitlab campaign (deprecated) | E-mail alias | Zendesk | ✓ |
| [E-mail (education@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail/) | Support contact | E-mail alias | Zendesk | ✓ |
| [E-mail (opensource@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail/) | Support contact | E-mail alias | Zendesk | ✓ |
| [E-mail (startups@gitlab.com)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail/) | Support contact | E-mail alias | Zendesk | ✓ |
| [E-mail (personal inbox)](/handbook/marketing/community-relations/community-advocacy/workflows/e-mail/) | E-mails to track as tickets | E-mail alias | Zendesk | ✓ |
| [Website: blog](/handbook/marketing/community-relations/community-advocacy/workflows/website-comments/) | Disqus comments | Zapier | Zendesk, #mentions-of-gitlab | ✓ |
| [Website: DevOps Tools](/handbook/marketing/community-relations/community-advocacy/workflows/devops-tools/) | Disqus comments | Zapier | Zendesk and Slack: #devops-tools-comments | ✓ |
| [Speakers](/events/find-a-speaker/) | Find-a-speaker form | Zapier | Zendesk | ✓ |
| [Reddit](/handbook/marketing/community-relations/community-advocacy/workflows/reddit/)  | Reddit mentions | Zapier | Zendesk | ✓ |
| [Documentation](/handbook/marketing/community-relations/community-advocacy/workflows/inactive/) | Disqus comments | Zapier | Slack: #docs-comments | ✓ (Docs Team)|
| [Stack Overflow](/handbook/marketing/community-relations/community-advocacy/workflows/stackoverflow/) | Stack Exchange mentions | Zapier | Zendesk | ✓ |
| [GitLab forum](/handbook/marketing/community-relations/community-advocacy/workflows/forum/) | forum.gitlab.com | Zapier | Zendesk and Slack: #gitlab-forum | ✓ |
| [Lobste.rs](/handbook/marketing/community-relations/community-advocacy/workflows/inactive/) | lobste.rs mentions | Zapier | Slack: #mentions-of-gitlab | ✖ |
| [IRC](/handbook/marketing/community-relations/community-advocacy/workflows/inactive/) | IRC support | N/A | N/A | ✖ |
| [Gitter](/handbook/marketing/community-relations/community-advocacy/workflows/inactive/) | Gitter support | N/A | N/A | ✖ |
| [YouTube](/handbook/marketing/community-relations/community-advocacy/workflows/inactive/) | YouTube comments | N/A | N/A | ✖ |
| [Mailing list](/handbook/marketing/community-relations/community-advocacy/workflows/inactive/) | GitLabHq Google Group (deprecated) | N/A | N/A | ✖ |
| [Quora](/handbook/marketing/community-relations/community-advocacy/workflows/inactive/) | GitLab Quora topic | N/A | N/A | ✖ |
| [Wider community content](/handbook/marketing/community-relations/community-advocacy/workflows/inactive/) | Blog post comments | N/A | N/A | ✖ |

## How we work

- [Community advocacy workflows](/handbook/marketing/community-relations/community-advocacy/workflows/)
- [Community advocacy guidelines](/handbook/marketing/community-relations/community-advocacy/guidelines/)
- [Pilot program page](/handbook/marketing/community-relations/community-advocacy/pilot-program/)

## <i class="fas fa-calendar-check fa-fw color-orange font-awesome" aria-hidden="true"></i> Coverage for important and/or urgent mentions

At this time, the Community Advocates team spans across three main timezones: CST (Chinese Standard Time, UTC +8), CET (Central European Time, UTC +1) and CDT (Central Daylight Time, UTC -5). Our typical coverage based on these time zones is Monday - Friday from 8:00UTC to 22:00UTC, plus occasional [weekend coverage for release days](/handbook/marketing/community-relations/community-advocacy/#release-day-advocate-duty).

While this gives us the capacity to address most mentions during the working day, often [important and/or urgent mentions](/handbook/marketing/community-relations/community-advocacy/guidelines/general/#urgent-and-important-mentions) happen outside our current coverage times.

### With the full team online

This is the ideal case where there is coverage from the full team and we follow the regular workflows for each one of our [monitored channels](/handbook/marketing/community-relations/community-advocacy/#community-response-channels).

**Community Advocates Coverage Handoff**: Before ending your day, please review ongoing conversations and mentions in our channels (especially #advocates-fyi in Slack) to assess whether anything could potentially become important and/or urgent. If that is the case:

- [Involve the relevant experts](/handbook/marketing/community-relations/community-advocacy/#involving-experts) as you would generally do.
- If there are any urgent tickets in Zendesk, be sure to share those in the `advocates-fyi` Slack channel.
- When your coverage begins each day, first check the `advocates-fyi` Slack group for any urgent updates, announcements, or links to tickets. After you've reviewed and taken necessary action on these tickets, start your regular processing workflow.


### With a team member offline

**Community Advocates**: If a member of the Community Advocates team is offline during their regular working hours (e.g. due to Paid Time Off, illness or unforeseen events) for a day or more, and that leaves their timezone uncovered, please activate the [Advocate for a day process](/handbook/marketing/community-relations/community-advocacy/#advocate-for-a-day) with at least two additional advocates.

If the time offline extends more than a few days, it is advisable to find additional advocates and rotate their roles.

### After hours

In general, if you notice an online mention that needs to be addressed, please ping `@advocates` on the [#community-advocates](https://gitlab.slack.com/messages/community-advocates) Slack channel. All [Community Advocates have notifications enabled for this group handle and this channel](/handbook/marketing/community-relations/community-advocacy/#respond-to-every-question-asked-internally) –notifications are also sent if the handle is mentioned on any GitLab Slack channel.

You can also [contact any of the Community Advocates](https://docs.google.com/document/d/1rjEKcIUC2H1HmPuQyijllVu044RxaEnRXH3GQKCsKyI/edit#heading=h.r01iolr373k3) directly via Slack or text message.

If required, please consider using the [Marketing Rapid Response Process](/handbook/marketing/#-marketing-rapid-response-process) as well.


## Deliverable scheduling

* Community Relations team meetings are on Tuesday.
* Community Advocates planning meetings are on Mondays and Wednesdays.
* For every handbook update (that substantially changes the content or layout), please follow the [handbook guidelines](/handbook/handbook-usage/#handbook-guidelines)

## Release day advocate duty

Every 22nd of the month we release a new version of GitLab. More often than not we get a spike in community mentions. To help deal with this we have dedicated release advocates that own the effort of responding to community mentions on/after a release.

Every month a different advocate has release advocate duty. It rotates on a monthly basis. If the release day takes place on a weekend, one of the advocates is assigned to monitor the traffic and to process mentions. We keep track of the assignments on the `Community Advocates` GitLab team calendar. You can view our [workflow page](/handbook/marketing/community-relations/community-advocacy/workflows/release-duties/) to see the advocates release day tasks.


- [Support handbook](/handbook/support/)
